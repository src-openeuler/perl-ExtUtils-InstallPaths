Name:		perl-ExtUtils-InstallPaths
Version:	0.013
Release:	2
Summary:	Build.PL install path logic made easy
License:	GPL-1.0-or-later OR Artistic-1.0-Perl
URL:		https://metacpan.org/release/ExtUtils-InstallPaths
Source0:	https://cpan.metacpan.org/modules/by-module/ExtUtils/ExtUtils-InstallPaths-%{version}.tar.gz
BuildArch:	noarch
BuildRequires:	coreutils findutils make perl-generators
BuildRequires:	perl-interpreter perl(ExtUtils::MakeMaker)
BuildRequires:	perl(ExtUtils::Config)

%description
This package contains the module of ExtUtils::InstallPaths that contains the
Build.PL file which could make install path easy.

%package_help

%prep
%autosetup -n ExtUtils-InstallPaths-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms}  %{buildroot}/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/ExtUtils/

%files help
%doc Changes
%{_mandir}/man3/ExtUtils::InstallPaths.3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 0.013-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jul 23 2024 gss <guoshengsheng@kylinos.cn> - 0.013-1
- Upgrade to version 0.013
- Try to install any installable paths

* Sat Feb 29 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.012-9
- Package init
